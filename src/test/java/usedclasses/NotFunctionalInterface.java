package usedclasses;

import java.util.List;

public abstract class NotFunctionalInterface {
    public abstract int f(double b);

    public abstract String h(List<String> stringList);
}
