import org.junit.Assert;
import org.junit.Test;
import usedclasses.ExtendsClass;
import usedclasses.FunctionalInterface;
import usedclasses.NotFunctionalInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestService {
    @Test
    public void testGetInfo(){
        Base base1 = new Base("base1 description");
        Derived base2 = new Derived("base2 description", "base2 additionDescription");
        Base base3 = new Base("base3 description");
        List<Base> actual = new ArrayList<>();
        Collections.addAll(actual, base1, base2, base3);

        List<String> expected = new ArrayList<>();
        Collections.addAll(expected, "base1 description", "base2 description, base2 additionDescription", "base3 description");

        Assert.assertEquals(expected, Service.getInfo(actual));
    }

    @Test
    public void testIsFunctionalInterface(){
        Assert.assertTrue(Service.isFunctionalInterface(FunctionalInterface.class));
        Assert.assertFalse(Service.isFunctionalInterface(NotFunctionalInterface.class));
        Assert.assertFalse(Service.isFunctionalInterface(String.class));
    }

    @Test
    public void testIsStringSerializable(){
        String serializable = new String("A");
        Assert.assertTrue(Service.isSerializable(serializable));
    }

    @Test
    public void testIsBaseSerializable(){
        Base notSerializable = new Base("A");
        Assert.assertFalse(Service.isSerializable(notSerializable));
    }

    @Test
    public void testIsExtendsClassSerializable(){
        ExtendsClass extendsClass = new ExtendsClass();
        Assert.assertTrue(Service.isSerializable(extendsClass));
    }

    @Test
    public void testGetStringCanonicalClassesNamesByStaticMethods(){
        List<String> expected = new ArrayList<>();
        Collections.addAll(expected, "Service", "java.util.Collections");

        List<Class<?>> actual = new ArrayList<>();
        Collections.addAll(actual, Service.class, Collections.class, Base.class);//Base has no static methods

        Assert.assertEquals(expected, Service.getStringCanonicalClassesNamesByStaticMethods(actual));
    }
}
